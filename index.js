'use strict';

const Koa = require('koa');
const koaBody = require('koa-body');
const respond = require('koa-respond');

const paginate = require('./middlewares/paginate');
const errorHandler = require('./middlewares/errorHandler');

const config = require('./config');

/**
 * ############## MIDDLEWARES ##############
 */

const app = new Koa();

app.use(koaBody());
app.use(respond());
app.use(errorHandler());
app.use(paginate());

/**
 * ############## ROUTES ##############
 */
const v1Routes = require('./routes/v1');

app.use(v1Routes.routes());
app.use(v1Routes.allowedMethods());

/**
 * ############## SERVER CONFIGURATION ##############
 */
const port = process.env.PORT || config.get('PORT');
const server = require('http').createServer(app.callback());

server.listen(port, () => {
    console.info(`Server is running on port  ${port}`);
});
