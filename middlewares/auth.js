'use strict';

const _ = require('lodash');
const Passport = require('passport-jwt');
const passport = require('koa-passport');

const { User } = require('../data/models');

const JwtStrategy = Passport.Strategy;
const ExtractJwt = Passport.ExtractJwt;

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'secretKey'
};

passport.use(
    new JwtStrategy(jwtOptions, async (payload, done) => {
        try {
            const user = await User.findOne({ where: { id: payload.id }, raw: true });

            if (_.isEmpty(user) || user.accessTokenSalt !== payload.salt) {
                return done(null, null);
            }

            done(null, user);
        } catch (e) {
            done(e, null);
        }
    })
);

module.exports = passport.authenticate('jwt', { session: false });
